package ee.ut.eventstr.model;

import java.awt.Image;
import java.math.BigInteger;
import java.util.List;

public class ProDriftDetectionResult {

	
	private Image pValuesDiagram;
	private List<BigInteger> driftPoints;
	private List<BigInteger> lastReadTrace;
	
	
	public Image getpValuesDiagram() {
		return pValuesDiagram;
	}
	public void setpValuesDiagram(Image pValuesDiagram) {
		this.pValuesDiagram = pValuesDiagram;
	}
	public List<BigInteger> getDriftPoints() {
		return driftPoints;
	}
	public void setDriftPoints(List<BigInteger> driftPoints) {
		this.driftPoints = driftPoints;
	}
	public List<BigInteger> getLastReadTrace() {
		return lastReadTrace;
	}
	public void setLastReadTrace(List<BigInteger> lastReadTrace) {
		this.lastReadTrace = lastReadTrace;
	}
	
	
}
