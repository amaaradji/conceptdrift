package ee.ut.eventstr.test;

import java.awt.Image;
import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import javax.imageio.ImageIO;

import jsc.contingencytables.ContingencyTable;
import jsc.independentsamples.MannWhitneyTest;
import jsc.independentsamples.SmirnovTest;

import org.deckfour.xes.extension.XExtension;
import org.deckfour.xes.extension.std.XConceptExtension;
import org.deckfour.xes.factory.XFactory;
import org.deckfour.xes.factory.XFactoryRegistry;
import org.deckfour.xes.model.XAttribute;
import org.deckfour.xes.model.XAttributeMap;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.jbpt.utils.IOUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.annotations.XYShapeAnnotation;
import org.jfree.chart.plot.XYPlot;

import ee.ut.eventstr.PrimeEventStructure;
import ee.ut.eventstr.model.ProDriftDetectionResult;
import ee.ut.eventstr.util.LinePlot;
import ee.ut.eventstr.util.MeanDelayCurve;
import ee.ut.eventstr.util.PrecisionRecallCurve;
import ee.ut.eventstr.util.confusionMat;
import ee.ut.mining.log.AlphaRelations;
import ee.ut.mining.log.PartiallyOrderedRun;
import ee.ut.mining.log.XLogReader;

enum DriftConfig{
	// structure used of the concept drift detection method 
	TRACE,
	RUN,
	PES,
	COMPONENT,
	BRANCHING
}

enum StatisticTestConfig{
	KS,
	MW,
	QS
}

enum WindowConfig{
	FWIN,
	ADWIN
}

public class AlphaBasedPosetReaderTest {

	
	public static void main(final String[] args) throws Exception {
//		changePatternTest();
//		changePatternSimpleTestChangeWise();

		
		boolean run = true;
		File logfile = null;
		int winsize = 100;
		DriftConfig cf = DriftConfig.RUN;
		WindowConfig wincf = WindowConfig.ADWIN;
		
		
		
//		if (true) {
//			
//			AlphaBasedPosetReaderTest driftTest0 = new AlphaBasedPosetReaderTest(readLog(new FileInputStream(logfile)), 100, cf, StatisticTestConfig.QS, WindowConfig.ADWIN);
//			driftTest0.test();
//			return;
//		}
		
//		switch (args.length) {
//		case 0:
//			System.out.println("to run this applicaiton, you need to provide at least one parameter (logfile path)");
//			break;
//		case 1:
//			logfile = new File(args[0]);
//			run = true;
//			break;
//		case 2:
//			logfile = new File(args[0]);
//			try { 
//				winsize=Integer.parseInt(args[1]); 
//				run = true;
//		    } catch(NumberFormatException e) { 
//		        System.out.println("the second parameter (window size) needs to be an integer");
//		    }
//			break;
//		case 3:
//			logfile = new File(args[0]);
//			try {
//				winsize = Integer.parseInt(args[1]);
//				run = true;
//			} catch (NumberFormatException e) {
//				System.out
//						.println("the second parameter (window size) needs to be an integer");
//				run = false;
//			}
//			if (args[2].toLowerCase() == "-fwin") {
//				wincf = WindowConfig.FWIN;
//				run = true;
//			} else {
//				System.out
//						.println("the third parameter (window configuration) to be an -fwin (if you want adaptive window dont specify this parametern)");
//				run = false;
//			}
//			break;
//		default:
//			System.out.println("this application take noe more than three parameters");
//			break;
//		}

		if (run) {
			
			Path path = Paths.get("./Loan_baseline_ORI.MXML");
			byte[] logByteArray = Files.readAllBytes(path);
			
			XLog xl = readLog(new ByteArrayInputStream(logByteArray), "Loan_baseline_ORI.MXML");
			AlphaBasedPosetReaderTest driftTest0 = new AlphaBasedPosetReaderTest(
					xl, winsize, cf, StatisticTestConfig.QS, wincf);
			
//			BufferedWriter writer;
//			
//			try {
//
//				writer = new BufferedWriter(new FileWriter(new File("./output.txt")));
//		         
//				
//				for(int i = 0; i < xl.size(); i++)
//				{
//					
//					for(int j=0; j < xl.get(i).size(); j++)
//					{
//						
//						writer.write(xl.get(i).get(j).getID() + ", ");
//						
//					}
//					writer.write("\n");
//					
//				}
//		        
//		        writer.close();
//			
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			
			driftTest0.test();
		}
//		driftTest0.winSizeTest();
//		driftTest0.PRcurves.plot();
//		driftTest0.MDcurves.plot();
//		System.out.println("***END MAIN***");
	}
	
	
	
	public ProDriftDetectionResult proDriftRun()
	{
		
		Image img= this.test();
		
		ProDriftDetectionResult pddres = new ProDriftDetectionResult();
		pddres.setpValuesDiagram(img);
		pddres.setDriftPoints(MDcurves.getDriftPoints());
		pddres.setLastReadTrace(MDcurves.getLastReadTrace());
		
		return pddres;
		
	}
	
	
	public static XLog readLog(InputStream logFile, String name)
	{
		XLog lg = null;
		try {
			lg = XLogReader.openLog(logFile, name);
		} catch (Exception e) {
			return null;
		}
		
		return lg;
		
	}
	
	public static boolean validateLog(InputStream logFile, String name)
	{
		XLog lg = null;
		try {
			lg = XLogReader.openLog(logFile, name);
		} catch (Exception e) {
			return false;
		}
		
		return true;
		
	}
	
	
	
	
	
	public static void changePatternSimpleTestChangeWise() throws FileNotFoundException {
		String[] logtype = new String[]{"re","cf","lp","pl","cb","cm","cp","cd","pm","rp","sw","fr","IOR","IRO","OIR","ORI","RIO","ROI"};
//		String[] logtype = new String[]{"IOR","IRO","OIR","ORI","RIO","ROI"};
//		String[] logtype = new String[]{"OIR"};
		String[] loglengthType = new String[]{"2.5k","5k","7.5k","10k"};
		int[] logsize = new int []{2500,5000,7500,10000};
		int[] winsize = new int []{25,50,75,100,125,150};
		WindowConfig[] config = new WindowConfig[]{WindowConfig.FWIN, WindowConfig.ADWIN};
		String res = "";
//		double percentage = 0.01;
		for (int t = 0; t < logtype.length; t++) {
			res += "\n\n"+ logtype[t]+ "\nsize, R_fscore, R_prec, R_recall, T_delay, R_delay, Avg-runTime(�s), , WinConfig, winsize \n";
			for (int l = 0; l < loglengthType.length; l++) {
				res+="\n";
				for (int c = 0; c < config.length; c++) {				
					int w = 0;
					//if (config[c] == WindowConfig.ADWIN) w = 3;
					for (; w < winsize.length; w++) {
						String logname = logtype[t]+loglengthType[l];
		//				int winsize = (int) (logsize[i] * percentage);
						System.out.println("--- A test for "+logname+" "+winsize[w]);
						AlphaBasedPosetReaderTest driftTest0 = new AlphaBasedPosetReaderTest(readLog(new FileInputStream(logname), logname),winsize[w], DriftConfig.RUN, StatisticTestConfig.QS, config[c]);
//						int distinctruns =  driftTest0.test(); //can be an output to the excel
		//				res += logname + "," + driftTest0.T_confMat.getFScore() + "," + driftTest0.T_confMat.getPrecision() + "," + driftTest0.T_confMat.getRecall() 
		//						+ "," + driftTest0.R_confMat.getFScore() + "," + driftTest0.R_confMat.getPrecision() + "," + driftTest0.R_confMat.getRecall() 
		//						+ "," + driftTest0.T_meandelay + "," + driftTest0.R_meandelay 
		//						+ "," + driftTest0.OverallTime/1000000/logsize[i] + "\n";
						res += loglengthType[l] + "," + driftTest0.R_confMat.getFScore() + "," + driftTest0.R_confMat.getPrecision() + "," + driftTest0.R_confMat.getRecall() 
								+ "," + driftTest0.T_meandelay + "," + driftTest0.R_meandelay 
								+ "," + (double)driftTest0.OverallTime/1000/logsize[l] + ", ," + config[c] + "," + winsize[w] + "\n";
					}
				}
			}
		}
		
		IOUtils.toFile("test.csv", res);
	}
	
	public static void changePatternSimpleTestLengthWise() throws FileNotFoundException {
		String[] logtype = new String[]{"br","bp","bc","bl"};
		String[] loglengthType = new String[]{"5k","10k","15k","20k"};
		int[] logsize = new int []{5000,10000,15000,20000};
		String res = "w50 \nlogname, T_fscore, T_prec, T_recall, R_fscore, R_prec, R_recall, T_delay, R_delay, Avg-runTime(ms) \n";
		for (int j = 0; j < logtype.length; j++) {
			for (int i = 0; i < loglengthType.length; i++) {
				String logname = logtype[j]+loglengthType[i];
				int winsize = 50;
				AlphaBasedPosetReaderTest driftTest0 = new AlphaBasedPosetReaderTest(readLog(new FileInputStream(logname), logname),winsize, DriftConfig.TRACE, StatisticTestConfig.QS, WindowConfig.ADWIN);
				driftTest0.test();
				res += logname + "," + driftTest0.T_confMat.getFScore() + "," + driftTest0.T_confMat.getPrecision() + "," + driftTest0.T_confMat.getRecall() 
						+ "," + driftTest0.R_confMat.getFScore() + "," + driftTest0.R_confMat.getPrecision() + "," + driftTest0.R_confMat.getRecall() 
						+ "," + driftTest0.T_meandelay + "," + driftTest0.R_meandelay 
						+ "," + driftTest0.OverallTime/1000000/logsize[i] + "\n";
			}
		}
		IOUtils.toFile("test.csv", res);
	}
	
	public static void changePatternTest() throws FileNotFoundException {
		String[] logtype = new String[]{"br","bp","bc","bl"};
		String[] loglengthType = new String[]{"5k","10k","15k","20k"};
		int[] logsize = new int []{5000,10000,15000,20000};
		String res = "0.1% \nlogname, T_fscore, T_prec, T_recall, R_fscore, R_prec, R_recall, T_delay, R_delay, Avg-runTime(ms) \n";
		for (int i = 0; i < loglengthType.length; i++) {
			for (int j = 0; j < logtype.length; j++) {
				String logname = logtype[j]+loglengthType[i];
				int winsize = (int) (logsize[i] * 0.001);
				AlphaBasedPosetReaderTest driftTest0 = new AlphaBasedPosetReaderTest(readLog(new FileInputStream(logname), logname), winsize, DriftConfig.TRACE, StatisticTestConfig.QS, WindowConfig.ADWIN);
				driftTest0.test();
				res += logname + "," + driftTest0.T_confMat.getFScore() + "," + driftTest0.T_confMat.getPrecision() + "," + driftTest0.T_confMat.getRecall() 
						+ "," + driftTest0.R_confMat.getFScore() + "," + driftTest0.R_confMat.getPrecision() + "," + driftTest0.R_confMat.getRecall() 
						+ "," + driftTest0.T_meandelay + "," + driftTest0.R_meandelay 
						+ "," + driftTest0.OverallTime/1000000/logsize[i] + "\n";
			}
		}
		
		res += "0.5%\nlogname, T_fscore, T_prec, T_recall, R_fscore, R_prec, R_recall, T_delay, R_delay, Avg-runTime(ms) \n";
		for (int i = 0; i < loglengthType.length; i++) {
			for (int j = 0; j < logtype.length; j++) {
				String logname = logtype[j]+loglengthType[i];
				int winsize = (int) (logsize[i] * 0.005);
				AlphaBasedPosetReaderTest driftTest0 = new AlphaBasedPosetReaderTest(readLog(new FileInputStream(logname), logname), winsize, DriftConfig.TRACE, StatisticTestConfig.QS, WindowConfig.ADWIN);
				driftTest0.test();
				res += logname + "," + driftTest0.T_confMat.getFScore() + "," + driftTest0.T_confMat.getPrecision() + "," + driftTest0.T_confMat.getRecall() 
						+ "," + driftTest0.R_confMat.getFScore() + "," + driftTest0.R_confMat.getPrecision() + "," + driftTest0.R_confMat.getRecall() 
						+ "," + driftTest0.T_meandelay + "," + driftTest0.R_meandelay 
						+ "," + driftTest0.OverallTime/1000000/logsize[i] + "\n";
			}
		}
		
		res += "1%\nlogname, T_fscore, T_prec, T_recall, R_fscore, R_prec, R_recall, T_delay, R_delay, Avg-runTime(ms) \n";
		for (int i = 0; i < loglengthType.length; i++) {
			for (int j = 0; j < logtype.length; j++) {
				String logname = logtype[j]+loglengthType[i];
				int winsize = (int) (logsize[i] * 0.01);
				AlphaBasedPosetReaderTest driftTest0 = new AlphaBasedPosetReaderTest(readLog(new FileInputStream(logname), logname), winsize, DriftConfig.TRACE, StatisticTestConfig.QS, WindowConfig.ADWIN);
				driftTest0.test();
				res += logname + "," + driftTest0.T_confMat.getFScore() + "," + driftTest0.T_confMat.getPrecision() + "," + driftTest0.T_confMat.getRecall() 
						+ "," + driftTest0.R_confMat.getFScore() + "," + driftTest0.R_confMat.getPrecision() + "," + driftTest0.R_confMat.getRecall() 
						+ "," + driftTest0.T_meandelay + "," + driftTest0.R_meandelay 
						+ "," + driftTest0.OverallTime/1000000/logsize[i] + "\n";
			}
		}
		
		res += "w=10\nlogname, T_fscore, T_prec, T_recall, R_fscore, R_prec, R_recall, T_delay, R_delay, Avg-runTime(ms) \n";
		for (int i = 0; i < loglengthType.length; i++) {
			for (int j = 0; j < logtype.length; j++) {
				String logname = logtype[j]+loglengthType[i];
				int winsize = 10;
				AlphaBasedPosetReaderTest driftTest0 = new AlphaBasedPosetReaderTest(readLog(new FileInputStream(logname), logname), winsize, DriftConfig.TRACE, StatisticTestConfig.QS, WindowConfig.ADWIN);
				driftTest0.test();
				res += logname + "," + driftTest0.T_confMat.getFScore() + "," + driftTest0.T_confMat.getPrecision() + "," + driftTest0.T_confMat.getRecall() 
						+ "," + driftTest0.R_confMat.getFScore() + "," + driftTest0.R_confMat.getPrecision() + "," + driftTest0.R_confMat.getRecall() 
						+ "," + driftTest0.T_meandelay + "," + driftTest0.R_meandelay 
						+ "," + driftTest0.OverallTime/1000000/logsize[i] + "\n";
			}
		}
		res += "w=50\nlogname, T_fscore, T_prec, T_recall, R_fscore, R_prec, R_recall, T_delay, R_delay, Avg-runTime(ms) \n";
		for (int i = 0; i < loglengthType.length; i++) {
			for (int j = 0; j < logtype.length; j++) {
				String logname = logtype[j]+loglengthType[i];
				int winsize = 50;
				AlphaBasedPosetReaderTest driftTest0 = new AlphaBasedPosetReaderTest(readLog(new FileInputStream(logname), logname), winsize, DriftConfig.TRACE, StatisticTestConfig.QS, WindowConfig.ADWIN);
				driftTest0.test();
				res += logname + "," + driftTest0.T_confMat.getFScore() + "," + driftTest0.T_confMat.getPrecision() + "," + driftTest0.T_confMat.getRecall() 
						+ "," + driftTest0.R_confMat.getFScore() + "," + driftTest0.R_confMat.getPrecision() + "," + driftTest0.R_confMat.getRecall() 
						+ "," + driftTest0.T_meandelay + "," + driftTest0.R_meandelay 
						+ "," + driftTest0.OverallTime/1000000/logsize[i] + "\n";
			}
		}
		res += "w=100\nlogname, T_fscore, T_prec, T_recall, R_fscore, R_prec, R_recall, T_delay, R_delay, Avg-runTime(ms) \n";
		for (int i = 0; i < loglengthType.length; i++) {
			for (int j = 0; j < logtype.length; j++) {
				String logname = logtype[j]+loglengthType[i];
				int winsize = 100;
				AlphaBasedPosetReaderTest driftTest0 = new AlphaBasedPosetReaderTest(readLog(new FileInputStream(logname), logname), winsize, DriftConfig.TRACE, StatisticTestConfig.QS, WindowConfig.ADWIN);
				driftTest0.test();
				res += logname + "," + driftTest0.T_confMat.getFScore() + "," + driftTest0.T_confMat.getPrecision() + "," + driftTest0.T_confMat.getRecall() 
						+ "," + driftTest0.R_confMat.getFScore() + "," + driftTest0.R_confMat.getPrecision() + "," + driftTest0.R_confMat.getRecall() 
						+ "," + driftTest0.T_meandelay + "," + driftTest0.R_meandelay 
						+ "," + driftTest0.OverallTime/1000000/logsize[i] + "\n";
			}
		}
		IOUtils.toFile("test.csv", res);
	}
	
	public void winSizeTest() {
		int logsize = log.size();
		System.out.println("log size "+logsize);
		int step_winsize = logsize / 200;
		System.out.println("step size" + step_winsize);
		for (int i = 1; i < 6; i++) {
			windowSize = step_winsize * i;
			System.out.println("excution for window size =" + windowSize);
			this.test();
		}
	}
	
	public AlphaBasedPosetReaderTest(XLog logFIle, int winsize, DriftConfig Dcfg, StatisticTestConfig statTest, WindowConfig winCfg) {
//		this.logFile =  logFilePath;
		this.windowSize = winsize;
		this.windowSizeReal = winsize;
		this.driftConfig = Dcfg;
		this.statisticTest = statTest;
		this.winConfig = winCfg;
		log = logFIle;
		
	}
	
	public AlphaBasedPosetReaderTest(InputStream logStream, String logName, int winsize, String fWinorAwin) {
		
		log = readLog(logStream, logName);
		this.windowSize = winsize;
		this.windowSizeReal = winsize;
		this.driftConfig = DriftConfig.RUN;
		this.statisticTest = statisticTest.QS;
		
		if(fWinorAwin.compareToIgnoreCase("FWIN") == 0)
			this.winConfig = WindowConfig.FWIN;
		else
			this.winConfig = WindowConfig.ADWIN;
		
		
	}
	
	DriftConfig driftConfig = DriftConfig.TRACE; 
	WindowConfig winConfig = WindowConfig.ADWIN;
	StatisticTestConfig statisticTest = StatisticTestConfig.QS;
	public static int numberOfDriftGoldStandar = 9; //beeing the number of event logs that i have concatinated 
	
	String logFile = "z_with_skip";
//	String logFile = "simrep12";
//	String logFile = "simrep12_2";
//	String logFile = "Process 63_98";
//	String logFile = "mg111FreqVariantFilter";
//	String logFile = "bpi_extract-v2";
//	String logFile = "ABCD-BC";
//	String logFile = "ABCD-ADCB";
//	String logFile = "z_with_skip_small";
//	String logFile = "teleclaims"; 
//	String logFile = "Artificial";
//	String logFile = "outTamplate";
	String fileName_trace  = "./%s.mxml";
//	String fileName_trace  = "./mxml/%s.xes";
	
	
	public static double typicalThreshold = 0.05;
	int windowSize = 100; // it has to be less than log.size/2
	double windowSizeReal = 100;
	List<XLog> eventLogList = new ArrayList<XLog>();
	XFactory factory = XFactoryRegistry.instance().currentDefault();
	
	
	XLog log;
	PrecisionRecallCurve PRcurves = new PrecisionRecallCurve();
	MeanDelayCurve MDcurves = new MeanDelayCurve();
	confusionMat T_confMat, R_confMat;
	double T_meandelay = 0, R_meandelay = 0;
	long OverallTime = 0;

	String driftSPOutput = "";
	String ratioDis ="";
	
 
	
	public Image test() {
		
		MDcurves.getDriftPoints().clear();
		MDcurves.getLastReadTrace().clear();
		
		LinePlot plotting = new LinePlot("P-value Diagram"+" (" +  "Window size = "+ windowSize + ", Window type = " + winConfig.toString() + ")", "Traces of log", "P-value"); //		

		int windowInitialSize = windowSize;
		
		
		long time = System.nanoTime();

		//splitLogIntoChunks(log,windowSize);
		
		AlphaRelations alphaRelations = new AlphaRelations(log);
		PartiallyOrderedRun strHandler = new PartiallyOrderedRun(alphaRelations, logFile, log);
		
		double pValue=0;
		
		switch (driftConfig) {
		case TRACE:	
			driftSPOutput += "TRACEs, ";
			int trCurveIndex = plotting.AddCurve("Traces");
			ArrayList<Double> tracepValVector = new ArrayList<>();
			
			strHandler.tracesToDisctrib();
			for (int i = 0; i < windowSize; i++) driftSPOutput += "1, ";
			for (int logIdex = 0; logIdex < log.size()-2*windowSize+1; logIdex++) {
				switch (statisticTest) {
				case KS:
					double [] spl1 = strHandler.getTracesSampleValuesDouble(logIdex,windowSize);
					double [] spl2 = strHandler.getTracesSampleValuesDouble(logIdex+windowSize,windowSize);
					SmirnovTest KSTest = new SmirnovTest(spl1, spl2);
					pValue = KSTest.getSP();
					break;
					
				case MW:
					spl1 = strHandler.getTracesSampleValuesDouble(logIdex,windowSize);
					spl2 = strHandler.getTracesSampleValuesDouble(logIdex+windowSize,windowSize);
					MannWhitneyTest MWTest = new MannWhitneyTest(spl1, spl2);
					pValue = MWTest.getSP();
					break;
					
				case QS:
					int [] splInt1 = strHandler.getTracesSampleValuesInt(logIdex,windowSize);
					int [] splInt2 = strHandler.getTracesSampleValuesInt(logIdex+windowSize,windowSize);
					int tracesFreqMatrixTemp[][] = new int[2][strHandler.existingTraces.size()];
					for (int i = 0; i < splInt1.length; i++) {
							tracesFreqMatrixTemp[0][splInt1[i]]+=1;
							tracesFreqMatrixTemp[1][splInt2[i]]+=1;
					}
					int countNonDoubleZero = 0;
					for (int i = 0; i < tracesFreqMatrixTemp[0].length; i++) {
						if (tracesFreqMatrixTemp[0][i]!=0 || tracesFreqMatrixTemp[1][i]!=0) 
							countNonDoubleZero++;
					}
					int tracesFreqMatrix[][] = new int[2][countNonDoubleZero];
					int index=0;
					for (int i = 0; i < tracesFreqMatrixTemp[0].length; i++) {
						if (tracesFreqMatrixTemp[0][i]!=0 || tracesFreqMatrixTemp[1][i]!=0){
							tracesFreqMatrix[0][index]=tracesFreqMatrixTemp[0][i];
							tracesFreqMatrix[1][index]=tracesFreqMatrixTemp[1][i];
							index++;
						}
					}
					ContingencyTable contingencyTable = new ContingencyTable(tracesFreqMatrix);
					jsc.contingencytables.ChiSquaredTest QSTest = new jsc.contingencytables.ChiSquaredTest(contingencyTable);
//					if (QSTest.hasSmallExpectedFrequency()) 
//						System.out.println("CHI-Squared test unreliable at "+logIdex);
//					driftSPOutput += QSTest.chiSquareDataSetsComparison(splInt1,splInt2) + ", ";
					if (QSTest.hasSmallExpectedFrequency()) System.out.println("WROOOOOONG TEST");
					pValue = QSTest.getSP();
					break;

				default:
					break;
				}
				
				plotting.addEleVal(trCurveIndex, logIdex+windowInitialSize, pValue);
				tracepValVector.add(pValue);
				driftSPOutput += pValue + ", ";
//				if (pValue<=0.05) System.out.println("trace-based drift at:"+(logIdex+windowSize));

//				XTrace trace = log.get(logIdex);
//				strHandler.addTrace(trace);
			}
			for (int i = 0; i < windowSize; i++) driftSPOutput += "1, ";
//			PRcurves.AddPRCurve("Traces-PR-"+windowSize, windowSize,tracepValVector);
//			PRcurves.AddPRLogCurve("Traces-PR-"+windowSize, windowSize, tracepValVector);
//			PRcurves.AddPRLogInvCurve("Traces-PR-"+windowSize, windowSize, tracepValVector);
//			MDcurves.AddMDCurve("Traces-MD-"+windowSize,windowSize,tracepValVector);
//			T_confMat = MDcurves.getConfMat(windowSize, tracepValVector, typicalThreshold);
//			T_meandelay = MDcurves.getMeanDelay(windowSize, tracepValVector, typicalThreshold);
			System.out.println("THE FSCORE IS "+T_confMat.getFScore());
//			IOUtils.toFile(model + "_SingleTraces.csv", driftSPOutput);
//			break;
		case RUN:	
			driftSPOutput += "\nRUNS, ";
			int runCurveIndex = plotting.AddCurve("P-value curve");
			ArrayList<Double> runpValVector = new ArrayList<>();
			ArrayList<Integer> winSizeVector = new ArrayList<>();
			strHandler.runsToDisctrib();
			
			//------- ADWIN
			int [] oldsplInt1 = strHandler.getRunsSampleValuesInt(0,windowSize);
			int [] oldsplInt2 = strHandler.getRunsSampleValuesInt(0+windowSize,windowSize);
			int oldrunsFreqMatrixTemp[][] = new int[2][strHandler.DistinctRuns.size()];
			for (int i = 0; i < oldsplInt1.length; i++) {
				oldrunsFreqMatrixTemp[0][oldsplInt1[i]]+=1;
				oldrunsFreqMatrixTemp[1][oldsplInt2[i]]+=1;
			}
			int oldcountNonDoubleZero = 0, oldcountNonRefZero = 0, oldcountNonDetZero = 0;
			for (int i = 0; i < oldrunsFreqMatrixTemp[0].length; i++) {
				if (oldrunsFreqMatrixTemp[0][i]!=0 || oldrunsFreqMatrixTemp[1][i]!=0) 
					oldcountNonDoubleZero++;
				if (oldrunsFreqMatrixTemp[0][i]!=0 ) oldcountNonRefZero++;
				if (oldrunsFreqMatrixTemp[1][i]!=0 ) oldcountNonDetZero++;

			}
			double oldRefRatio = (double) oldcountNonRefZero / windowSize;
			double oldDetRatio = (double) oldcountNonDetZero / windowSize;
			int oldWinSize = windowSize;
			//------- ADWIN
			
			Queue<Double> total = new LinkedList<Double>(); 
			
			for (int i = 0; i < windowSize; i++) driftSPOutput += "1, ";
			for (int logIdex = 0; logIdex < log.size()-2*windowSize+1; logIdex++) {
				switch (statisticTest) {
				case KS:
					double [] spl1 = strHandler.getRunsSampleValuesDouble(logIdex,windowSize);
					double [] spl2 = strHandler.getRunsSampleValuesDouble(logIdex+windowSize,windowSize);
					SmirnovTest KSTest = new SmirnovTest(spl1, spl2);
					pValue = KSTest.getSP();
					break;
					
				case MW:
					spl1 = strHandler.getRunsSampleValuesDouble(logIdex,windowSize);
					spl2 = strHandler.getRunsSampleValuesDouble(logIdex+windowSize,windowSize);
					MannWhitneyTest MWTest = new MannWhitneyTest(spl1, spl2);
					pValue = MWTest.getSP();
					break;
					
				case QS:
					int [] splInt1 = strHandler.getRunsSampleValuesInt(logIdex,windowSize);
					int [] splInt2 = strHandler.getRunsSampleValuesInt(logIdex+windowSize,windowSize);
					int countNonDoubleZero = 0, countNonRefZero = 0, countNonDetZero = 0;
					if (this.winConfig == WindowConfig.ADWIN){
						int newrunsFreqMatrixTemp[][] = new int[2][strHandler.DistinctRuns.size()];
						for (int i = 0; i < splInt1.length; i++) {
							newrunsFreqMatrixTemp[0][splInt1[i]]+=1;
							newrunsFreqMatrixTemp[1][splInt2[i]]+=1;
						}

						for (int i = 0; i < newrunsFreqMatrixTemp[0].length; i++) {
							if (newrunsFreqMatrixTemp[0][i]!=0 || newrunsFreqMatrixTemp[1][i]!=0) 
								countNonDoubleZero++;
							if (newrunsFreqMatrixTemp[0][i]!=0 ) countNonRefZero++;
							if (newrunsFreqMatrixTemp[1][i]!=0 ) countNonDetZero++;
	
						}
						
	
						//----ADWIN

						double refRatio = (double) countNonRefZero / windowSize, detRatio = (double) countNonDetZero / windowSize;
						double percentageRef =  refRatio/oldRefRatio;
						double percentageDet =  detRatio/oldDetRatio;
						double avgPercentage = (percentageRef+percentageDet)/2;
						if (total.size() == 10 ) total.poll();//smoothing effect
						total.add(percentageDet);
		
						oldRefRatio = refRatio; 
						oldDetRatio = detRatio;
						
						double sum = 0;
						for (Iterator iterator = total.iterator(); iterator
								.hasNext();) {
							sum  += (Double) iterator.next();
						}
						double avgTotal = sum / total.size();
						
						windowSizeReal = Math.max(Math.min( windowSizeReal * avgTotal,400),10);
						windowSize = (int) Math.round(windowSizeReal);
						if ( logIdex + 2*windowSize > log.size()) 
							break;
//						windowSize = (int) Math.max(Math.min( Math.round(windowSize * avgTotal),400),10); //adapting windowsize
//						if (percentageRef>1 && percentageDet>1) windowSize = (int) Math.max(Math.min( Math.round(windowSize * avgPercentage),200),10);
//						if (percentageRef<1 && percentageDet<1) windowSize = (int) Math.max(Math.min( Math.round(windowSize * avgPercentage),200),10);
	
						
						oldWinSize = windowSize;
						
						ratioDis += countNonDetZero + "," + percentageRef + "," + percentageDet + "," + avgTotal  + "," + windowSize + "," + windowSizeReal + "\n";
						
						countNonDoubleZero = 0; countNonRefZero = 0; countNonDetZero = 0;
											
//						if (windowSize>500)
//							System.out.println("WINDOW SIZE " + windowSize + " TOO BIG at "+ logIdex );
//						if (windowSize<50)
//							System.out.println("window size " + windowSize + " too smll at "+ logIdex );
//						if (logIdex >= 9000)
//							System.out.println(logIdex + " ------------- " + windowSize + " ----- " + avgPercentage);
					}
					
					
					//----ADWIN
					

					splInt1 = strHandler.getRunsSampleValuesInt(logIdex,windowSize);
					splInt2 = strHandler.getRunsSampleValuesInt(logIdex+windowSize,windowSize);
					int [][] runsFreqMatrixTemp = new int[2][strHandler.DistinctRuns.size()];
					for (int i = 0; i < splInt1.length; i++) {
						runsFreqMatrixTemp[0][splInt1[i]]+=1;
						runsFreqMatrixTemp[1][splInt2[i]]+=1;
					}
					for (int i = 0; i < runsFreqMatrixTemp[0].length; i++) {
						if (runsFreqMatrixTemp[0][i]!=0 || runsFreqMatrixTemp[1][i]!=0) 
							countNonDoubleZero++;
						if (runsFreqMatrixTemp[0][i]!=0 ) countNonRefZero++;
						if (runsFreqMatrixTemp[1][i]!=0 ) countNonDetZero++;

					}
					
					int runsFreqMatrix[][] = new int[2][countNonDoubleZero];
					int index=0;
					for (int i = 0; i < runsFreqMatrixTemp[0].length; i++) {
						if (runsFreqMatrixTemp[0][i]!=0 || runsFreqMatrixTemp[1][i]!=0){
							runsFreqMatrix[0][index]=runsFreqMatrixTemp[0][i];
							runsFreqMatrix[1][index]=runsFreqMatrixTemp[1][i];
							index++;
						}
					}
					//printTopRun(runsFreqMatrix);
					ContingencyTable contingencyTable = new ContingencyTable(runsFreqMatrix);
					if (contingencyTable.getColumnCount() <2)
						System.out.println("COLUMN COUNT UNDER 2 at  " + logIdex + " winsize " + windowSize ); 
					jsc.contingencytables.ChiSquaredTest QSTest = new jsc.contingencytables.ChiSquaredTest(contingencyTable);
//					if (QSTest.hasSmallExpectedFrequency()) 
//						System.out.println("CHI-Squared test unreliable at "+logIdex);
//					driftSPOutput += QSTest.chiSquareDataSetsComparison(splInt1,splInt2) + ", ";
					if (QSTest.hasSmallExpectedFrequency()) System.out.println("WROOOOOONG TEST");
					pValue = QSTest.getSP();
					break;

				default:
					break;
				}
				
				
				plotting.addEleVal(runCurveIndex, logIdex+windowInitialSize, pValue);
				runpValVector.add(pValue);
				winSizeVector.add(windowSize);
				driftSPOutput += pValue + ", ";
//				if (pValue<=0.05) System.out.println("run-based drift at:"+(logIdex+windowSize));
//				XTrace trace = log.get(logIdex);
//				strHandler.addTrace(trace);
			}
			for (int i = 0; i < windowSize; i++) driftSPOutput += "1, ";
//			IOUtils.toFile(model + "_SingleRuns.csv", driftSPOutput);
//			PRcurves.AddPRCurve("Runs-PR-"+windowSize, windowSize, runpValVector);
//			PRcurves.AddPRLogCurve("Runs-PR-"+windowSize, windowSize,runpValVector);
//			PRcurves.AddPRLogInvCurve("Runs-PR-"+windowSize, windowSize, runpValVector);
//			MDcurves.AddMDCurve("Runs-MD-"+windowSize,windowSize,runpValVector);
			R_confMat = MDcurves.getConfMat(windowSize, windowInitialSize, runpValVector, winSizeVector, typicalThreshold);
			R_meandelay = MDcurves.getMeanDelay(windowInitialSize, runpValVector, typicalThreshold);
//			System.out.println("THE FSCORE IS "+R_confMat.getFScore() + " -- Precision " + R_confMat.getPrecision()  + " -- Recall " + R_confMat.getRecall());
//			System.out.println("MeanDelay " + R_meandelay);
//			IOUtils.toFile("ratioDis.csv", ratioDis);
			break;
			
		default:
			break;
		}
		
//		IOUtils.toFile(model + "_SP_traces_runs.csv", driftSPOutput);

//		IOUtils.toFile(model + "_type_traces_runs.csv", strHandler.sampleTracesOutput);

//		for (int ind = 0; ind < eventLogList.size(); ind++) {
//			XLog LogChunk = factory.createLog();
//			LogChunk = (XLog)eventLogList.get(ind).clone();  
//			
//			
//			PartiallyOrderedRun.DRCount.add(ind, new HashMap<Integer,Integer>());
//
//			for (int i = 0; i < LogChunk.size(); i++) {
//				XTrace trace = LogChunk.get(i);
//				strHandler.addTrace(trace,ind);
//				
//				
//			}
//			
//			//IOUtils.toFile(ind + model + "_prefix.dot", runs.toDot());
//			//IOUtils.toFile(ind + model + "_DistinctRuns.dot", runs.DRtoDot());
//		
//		}
		
//		IOUtils.toFile(model + "_DistinctRuns.dot", strHandler.DRtoDot());		
		//IOUtils.toFile(model + "_Runs_freq.csv", strHandler.DRtoMatrixFile());
		
		
		
//		runs.mergePrefix();
//		IOUtils.toFile(model + "_merged.dot", runs.toDot());
//
//		
//		PrimeEventStructure<Integer> pes = runs.getPrimeEventStructure();
//		pes.pack();
//		dumpPES(model, pes);
		
		OverallTime = System.nanoTime() - time;
//		System.out.println("Overall time: " + (System.nanoTime() - time) / 1000000000.0);
	    plotting.addThreshold(typicalThreshold);
		
	    
	    JFreeChart lineChart = plotting.plot(MDcurves.getDriftPoints(), AlphaBasedPosetReaderTest.typicalThreshold);

	    

		
		BufferedImage objBufferedImage=lineChart.createBufferedImage(1024,600);
		
		Image img = objBufferedImage.getScaledInstance(-1, -1, Image.SCALE_DEFAULT);
//		ByteArrayOutputStream bas = new ByteArrayOutputStream();
//		        try {
//		            ImageIO.write(objBufferedImage, "png", bas);
//		        } catch (IOException e) {
//		            e.printStackTrace();
//		        }
//
//		byte[] byteArray=bas.toByteArray();
//		
//		InputStream in = new ByteArrayInputStream(byteArray);
//		BufferedImage image;
//		try {
////			image = ImageIO.read(in);
//			File outputfile = new File("./image.png");
//			ImageIO.write(objBufferedImage, "png", outputfile);
//
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
		
		
		
		
		
	    return img;
	}
	
	
	
//	@Test
//	public void test() throws Exception {
//		XLog log = XLogReader.openLog(String.format(fileName_trace, model));		
//		AlphaRelations alphaRelations = new AlphaRelations(log);
//		
//	    long time = System.nanoTime();
//		PartiallyOrderedRun runs = new PartiallyOrderedRun(alphaRelations, model);
//
//		for (int i = 0; i < log.size(); i++) {
//			XTrace trace = log.get(i);
//			runs.addTrace(trace);
//			
//		}
//		
//		IOUtils.toFile(model + "_prefix.dot", runs.toDot());
//		IOUtils.toFile(model + "_DistinctRuns.dot", runs.DRtoDot());
//		runs.mergePrefix();
//		IOUtils.toFile(model + "_merged.dot", runs.toDot());
//
//		
//		PrimeEventStructure<Integer> pes = runs.getPrimeEventStructure();
//		pes.pack();
//		dumpPES(model, pes);
//		
//	    System.out.println("Overall time: " + (System.nanoTime() - time) / 1000000000.0);
//	}
	
	private void printTopRun(int[][] runsFreqMatrix) {
		// TODO Auto-generated method stub
		
	}

	public void dumpPES(String model, PrimeEventStructure<Integer> pes)
			throws FileNotFoundException {
		System.out.println("Done with pes");
	    PrintStream out = null;
	    out = new PrintStream("target/"+model+".pes.tex");
	    pes.toLatex(out);
	    out.close();
	    System.out.println("Done with toLatex");
	}
	
	
	private void splitLogIntoChunks(XLog log, int splitSize){
		eventLogList.clear();
		int noTraces = log.size();
		int noChunks = (int)Math.ceil((1.0*noTraces)/splitSize);
		
		XFactory factory = XFactoryRegistry.instance().currentDefault();
		XAttributeMap attributeMap;
		List<XTrace> chunkTraceList;
		XLog chunkLog;
		int sumChunks = 0;
		int noEventsInLog;
		for(int i = 0; i < noChunks; i++){
			noEventsInLog = 0;
			chunkLog = factory.createLog();
			for (XExtension extension : log.getExtensions())
				chunkLog.getExtensions().add(extension);

			attributeMap = (XAttributeMap)log.getAttributes().clone();
			XAttribute conceptNameAttribute = factory.createAttributeLiteral("concept:name", attributeMap.get("concept:name").clone().toString()+"_Split_"+i, XConceptExtension.instance());
			attributeMap.put("concept:name", conceptNameAttribute);
			
			chunkLog.setAttributes(attributeMap);
			
			if(i < noChunks-1)
				chunkTraceList = log.subList(i*splitSize, (i+1)*splitSize);
			else
				chunkTraceList = log.subList((noChunks-1)*splitSize, noTraces);
			for(XTrace trace : chunkTraceList){
				chunkLog.add((XTrace)trace.clone());
				noEventsInLog += trace.size();
			}
			sumChunks += chunkLog.size();
			eventLogList.add((XLog)chunkLog.clone());
			System.out.println("No. Events in Log: "+noEventsInLog);
		}
		
		System.out.println("Original No. Traces: "+noTraces);
		System.out.println("No. Chunks: "+noChunks);
		System.out.println("Sum of Chunks: "+sumChunks);
	}
	
	
}
