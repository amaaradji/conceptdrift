package ee.ut.eventstr.util;

import java.awt.geom.Ellipse2D;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.annotations.XYShapeAnnotation;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;

public class LinePlot extends ApplicationFrame{
	
	XYSeriesCollection dataset = new XYSeriesCollection();
	ArrayList<XYSeries> curves = new ArrayList<XYSeries>();
	String title,XLb,YLb;
	
	boolean addThr = false;
	
	
	XYSeries thrsLine = new XYSeries("Threshhold");
	
	public LinePlot(String tle,String Xlabel, String Ylabel) {
		super("ProDrift");
		title = tle;
		XLb = Xlabel;
		YLb = Ylabel;
	}
	
	//create new XYseries and return its index (beeing the last one) 
	public int AddCurve(String curveName) {
		XYSeries curvetoAdd = new XYSeries(curveName);
		curves.add(curvetoAdd);
		return curves.size()-1;
	}


	public void addEleVal(int curveIndex, double xval, double yval) {
		curves.get(curveIndex).add(xval,yval);
	}
	
	
	public void addThreshold (double trValue)
	{
		addThr = true;
		thrsLine.add(curves.get(0).getMinX(), trValue);
		thrsLine.add(curves.get(0).getMaxX(), trValue);
	}
	

	private XYDataset createDataset() {
		
		for (int i = 0; i < curves.size(); i++) {
			dataset.addSeries(curves.get(i));
		}
		
		if (addThr) dataset.addSeries(thrsLine);

		return dataset;
	}
	
	public JFreeChart plot() {
		JFreeChart lineChart = ChartFactory.createXYLineChart(title,XLb, YLb, createDataset(),PlotOrientation.VERTICAL ,
		         true , true , false);
			

		ChartPanel chartPanel = new ChartPanel(lineChart);
		chartPanel.setPreferredSize(new java.awt.Dimension(560, 367));
		setContentPane(chartPanel);
		this.pack();
		RefineryUtilities.centerFrameOnScreen(this);
		this.setVisible(true);
		
		return lineChart;
		
	}

	public JFreeChart plot(List<BigInteger> driftPoints, double threshold) {
		JFreeChart lineChart = ChartFactory.createXYLineChart(title,XLb, YLb, createDataset(),PlotOrientation.VERTICAL ,
		         true , true , false);
		
		for(int i = 0; i < driftPoints.size(); i++)
		{
			
			((XYPlot) lineChart.getPlot()).addAnnotation(new XYShapeAnnotation(new Ellipse2D.Double(((double)driftPoints.get(i).intValue()) - 10, 
					threshold - 0.005, 20, 0.01)));
			
		}
		

		ChartPanel chartPanel = new ChartPanel(lineChart);
		chartPanel.setPreferredSize(new java.awt.Dimension(560, 367));
		setContentPane(chartPanel);
		this.pack();
		RefineryUtilities.centerFrameOnScreen(this);
		this.setVisible(true);
		
		return lineChart;
		
	}
}