package ee.ut.eventstr.util;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import com.aliasi.classify.ScoredPrecisionRecallEvaluation;

import ee.ut.eventstr.test.AlphaBasedPosetReaderTest;

public class MeanDelayCurve {

	LinePlot plotMD;
	
	List<BigInteger> driftPoints = new ArrayList<>();
	List<BigInteger> lastReadTrace = new ArrayList<>();
	List<Double> pValuesAtDrifts = new ArrayList<>();
	
	public MeanDelayCurve() {
		plotMD = new LinePlot("Mean Delay", "Threshold", "Delay");
		}

	public void plot() {
		plotMD.plot();
	}
	
	
	
	
	public List<Double> getpValuesAtDrifts() {
		return pValuesAtDrifts;
	}

	public void setpValuesAtDrifts(List<Double> pValuesAtDrifts) {
		this.pValuesAtDrifts = pValuesAtDrifts;
	}

	public List<BigInteger> getDriftPoints() {
		return driftPoints;
	}

	public void setDriftPoints(List<BigInteger> driftPoints) {
		this.driftPoints = driftPoints;
	}

	public List<BigInteger> getLastReadTrace() {
		return lastReadTrace;
	}

	public void setLastReadTrace(List<BigInteger> lastReadTrace) {
		this.lastReadTrace = lastReadTrace;
	}

	public void AddMDCurve(String title, int winsize , ArrayList<Double> pValVector) {
		int curveIndex = plotMD.AddCurve(title);
		
		double[] ThresholdList = new double[]{0.01,0.02,0.03,0.04,0.05,0.06,0.07,0.08,0.09,0.1,0.11,0.12,0.13,0.14,0.15,0.16,0.17,0.18,0.19};
		boolean[] FiredDrift = new boolean[]{false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false};
		ArrayList<ArrayList<Double>> delayList = new ArrayList<>();


		
		for (int tsIt = 0; tsIt < ThresholdList.length; tsIt++) {

			plotMD.addEleVal(curveIndex, ThresholdList[tsIt], getMeanDelay(winsize, pValVector, ThresholdList[tsIt]));
		}
	}
	
	public double getMeanDelay(int winsize , ArrayList<Double> pValVector, double Threshold){
		ArrayList<Boolean> Driftlabels = new ArrayList<>();
		
		int driftStep= (pValVector.size()-1+2*winsize) /(AlphaBasedPosetReaderTest.numberOfDriftGoldStandar+1);

		for (int i = winsize; i < pValVector.size()+winsize; i++) {
			if ((i+winsize)%driftStep== 0) 
				Driftlabels.add(false);
			else 
				Driftlabels.add(true);
		}	
		
		boolean FiredDrift = false;
		
		int countDrift = 0;


		
		int driftPoint = 0;
		double totalDelay = 0;
		for (int drIt = 0; drIt < pValVector.size(); drIt++) {
			if (!Driftlabels.get(drIt)) {//actual drift
				driftPoint = drIt;
				FiredDrift=true;//start search for next detected drift (pvalue<threshold)
			}
			if (pValVector.get(drIt) <= Threshold && FiredDrift){
				totalDelay+=(drIt-driftPoint);
				countDrift++;
				FiredDrift=false;//found
			}
		}
		return (double) totalDelay/countDrift;
	}
	
	
	public confusionMat getConfMat(int winsize , int windowInitialSize, ArrayList<Double> pValVector, ArrayList<Integer> winSizeVector, double threshold) {
		int tP=0,fP=0,fN=0;
		
		boolean FiredDrift = false;
		boolean computingLenght = false;
		int driftLenght=0;
		
		ArrayList<Boolean> Driftlabels = new ArrayList<>();	
		int driftStep= (pValVector.size()-1+2*winsize) /(AlphaBasedPosetReaderTest.numberOfDriftGoldStandar+1);
		for (int i = winsize; i < pValVector.size()+winsize; i++) {
			if ((i+winsize)%driftStep== 0) 
				Driftlabels.add(false);
			else 
				Driftlabels.add(true);
		}
		
		int pVVIt=0;
		int pVVIt_drift = -1;
		do {
			while (pVVIt < pValVector.size()
					&& pValVector.get(pVVIt) <= threshold) {
				if (!Driftlabels.get(pVVIt)) {// actual drift
						FiredDrift = true;// start search for next detected
											// drift (pvalue<threshold)
				}
				if(pVVIt_drift == -1)
					pVVIt_drift = pVVIt;
				driftLenght++;
				pVVIt++;
			}
			if (pVVIt < pValVector.size()) {
				if (driftLenght >= windowInitialSize / 3) {//todo abdu trick to filter false positive
					if (FiredDrift) {// found
						driftPoints.add(BigInteger.valueOf(pVVIt_drift + windowInitialSize + winSizeVector.get(pVVIt_drift)));
						lastReadTrace.add(BigInteger.valueOf(pVVIt_drift + windowInitialSize + winSizeVector.get(pVVIt_drift)));
						pValuesAtDrifts.add(pValVector.get(pVVIt_drift));
						System.out.println("drift detected at "+ (pVVIt_drift + windowInitialSize) +" after reading "+ (pVVIt_drift + windowInitialSize + winSizeVector.get(pVVIt_drift)) +" traces" + ", window size = " +winSizeVector.get(pVVIt_drift));
						tP++;
						FiredDrift = false;
					} else
						fP++;
				}
//				else if (FiredDrift && driftLenght>7) System.out.println("traces number is "+pVVIt + "length is " + driftLenght);
				if (!Driftlabels.get(pVVIt)) {// actual drift
						FiredDrift = true;// start search for next detected
											// drift (pvalue<threshold)
				}
				pVVIt_drift = -1;
				driftLenght = 0;
				pVVIt++;
			}
		} while (pVVIt < pValVector.size());
		
		int tN=0;
		fN = AlphaBasedPosetReaderTest.numberOfDriftGoldStandar - tP; 
		return new confusionMat(tP, fP, fN);
//		return (double)(2*tP)/(2*tP+fP+fN);
	}
	

	public double getBasicPrecisionNOTWORKING(int winsize , ArrayList<Double> pValVector, double threshold) {
		int tP=0,fP=0,fN=0;
		
		boolean FiredDrift = false;
		boolean computingLenght = false;
		int driftLenght=0;
		
		ArrayList<Boolean> Driftlabels = new ArrayList<>();	
		int driftStep= (pValVector.size()-1+2*winsize) /(AlphaBasedPosetReaderTest.numberOfDriftGoldStandar+1);
		for (int i = winsize; i < pValVector.size()+winsize; i++) {
			if ((i+winsize)%driftStep== 0) 
				Driftlabels.add(false);
			else 
				Driftlabels.add(true);
		}
		
		int pVVIt=0;
		for ( pVVIt = 0; pVVIt < pValVector.size(); pVVIt++) {
			boolean found = false;
			while (pValVector.get(pVVIt) <= threshold && pVVIt<pValVector.size()) {found = true;
			pVVIt++;}
			if (found) {tP++;
			found = false;}
		}
		
		int tN=0;
		return (double)tP/AlphaBasedPosetReaderTest.numberOfDriftGoldStandar;
	}
	
	
	public void AddMDCurve_depricated(String title, int winsize , ArrayList<Double> pValVector) {
		int curveIndex = plotMD.AddCurve(title);
		
		double[] ThresholdList = new double[]{0.01,0.02,0.03,0.04,0.05,0.06,0.07,0.08,0.09,0.1,0.11,0.12,0.13,0.14,0.15,0.16,0.17,0.18,0.19};
		boolean[] FiredDrift = new boolean[]{false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false};
		ArrayList<ArrayList<Double>> delayList = new ArrayList<>();
		for (int i = 0; i < ThresholdList.length; i++) delayList.add(new ArrayList<Double>());//initialize the list
		ArrayList<Boolean> Driftlabels = new ArrayList<>();
		
		int driftStep= (pValVector.size()-1+2*winsize) /(AlphaBasedPosetReaderTest.numberOfDriftGoldStandar+1);
//		Driftlabels.add(true);
		for (int i = winsize; i < pValVector.size()+winsize; i++) {
			if ((i+winsize)%driftStep== 0) 
				Driftlabels.add(false);
			else 
				Driftlabels.add(true);
		}	
//		Driftlabels.add(true);

		
		for (int tsIt = 0; tsIt < ThresholdList.length; tsIt++) {
			int driftPoint = 0;
			double totalDelay = 0;
			for (int drIt = 0; drIt < pValVector.size(); drIt++) {
				if (!Driftlabels.get(drIt)) {//actual drift
					driftPoint = drIt;
					FiredDrift[tsIt]=true;//start search for next detected drift (pvalue<threshold)
				}
				if (pValVector.get(drIt) <= ThresholdList[tsIt] && FiredDrift[tsIt]){
					delayList.get(tsIt).add((double)drIt-driftPoint);
					totalDelay+=(drIt-driftPoint);
					FiredDrift[tsIt]=false;//found
				}
			}
			plotMD.addEleVal(curveIndex, ThresholdList[tsIt], totalDelay/delayList.get(tsIt).size());
		}
	}
	
	
	
	public static void main(String[] args) {
		
		ArrayList<Double> Pvalue = new ArrayList<Double>();
		for (int i = 0; i < 100; i++) {
			Pvalue.add((double) new java.util.Random().nextFloat());
		}
		MeanDelayCurve md= new MeanDelayCurve();
		md.AddMDCurve("test",10,Pvalue);
		md.plot();
			
	}

}
