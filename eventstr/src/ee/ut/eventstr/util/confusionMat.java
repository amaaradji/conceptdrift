package ee.ut.eventstr.util;

public class confusionMat {
	
	public confusionMat(int tPos, int fPos, int fNeg) {
		tP=tPos;
		fP=fPos;
		fN=fNeg;
	}
	
	public int tP=0,fP=0,fN=0;
	public double getFScore() {
		return (double)(2*tP)/(2*tP+fP+fN);
	}
	
	public double getPrecision() {
		return (double)(tP)/(tP+fP);
	}
	
	public double getRecall() {
		return (double)(tP)/(tP+fN);
	}

}
