package ee.ut.mining.log;

import java.io.File;
import java.io.InputStream;

import org.deckfour.xes.in.XMxmlGZIPParser;
import org.deckfour.xes.in.XMxmlParser;
import org.deckfour.xes.in.XesXmlGZIPParser;
import org.deckfour.xes.in.XesXmlParser;
import org.deckfour.xes.model.XLog;

public class XLogReader {
	public static XLog openLog(InputStream inputLogFile, String name) throws Exception {
		XLog log = null;

		
		try{
			
			if(name.toLowerCase().contains("mxml.gz")){
				XMxmlGZIPParser parser = new XMxmlGZIPParser();
	//			if(parser.canParse()){
	//				try {
						log = parser.parse(inputLogFile).get(0);
	//				} catch (Exception e) {
	//					e.printStackTrace();
	//				}
	//			}
			}else if(name.toLowerCase().contains("mxml") || 
					name.toLowerCase().contains("xml")){
				XMxmlParser parser = new XMxmlParser();
	//			if(parser.canParse(inputLogFile)){
	//				try {
						log = parser.parse(inputLogFile).get(0);
	//				} catch (Exception e) {
	//					e.printStackTrace();
	//				}
	//			}
			}
	
			if(name.toLowerCase().contains("xes.gz")){
				XesXmlGZIPParser parser = new XesXmlGZIPParser();
	//			if(parser.canParse(inputLogFile)){
	//				try {
						log = parser.parse(inputLogFile).get(0);
	//				} catch (Exception e) {
	//					e.printStackTrace();
	//				}
	//			}
			}else if(name.toLowerCase().contains("xes")){
				XesXmlParser parser = new XesXmlParser();
	//			if(parser.canParse(inputLogFile)){
	//				try {
						log = parser.parse(inputLogFile).get(0);
	//				} catch (Exception e) {
	//					e.printStackTrace();
	//				}
	//			}
			}

		}catch(Exception e)
		{
			throw new Exception("File invalid");
		}

		if(log == null)
			throw new Exception("Oops ...");
		
		return log;
	}
}
